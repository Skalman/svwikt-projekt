<?php

ini_set('display_errors', '1');
error_reporting(E_ALL);

$method = $_SERVER['REQUEST_METHOD'];
$user = @$_GET['user'];
$list = @$_GET['list'];
$content = @$_POST['content'];

if ($method === 'POST') {
    $user = trim(@$_POST['user']);
    $list = trim(@$_POST['list']);
    $content = @$_POST['content'];
    $filename = "content/$user-$list.txt";
    $create = @$_POST['create'];
    $update = @$_POST['update'];
    $delete = @$_POST['delete'];

    $error = getSegmentError('user', $user);
    $error ??= getSegmentError('list', $list);
    if ($error) {
        err(400, $error);
    }

    $fileExists = file_exists($filename);

    if ($fileExists && $create) {
        err(400, 'Listan finns redan');
    }

    if (!$fileExists && ($update || $delete)) {
        err(404, 'Listan finns inte');
    }

    if ($content && !$delete) {
        if (strlen($content) > 1_000_000) {
            err(400, 'Listan är för lång');
        }

        $result = file_put_contents($filename, $content);
        if ($result === false) {
            err(500, 'Listan kunde inte sparas');
        }

        header('Location: ./?_=' . urlencode("$user-$list"));
    } else {
        @unlink($filename);
        deleted();
    }
} elseif (isset($_GET['_'])) {
    @[$user, $list] = explode('-', $_GET['_'], 2);
    $filename = "content/$user-$list.txt";

    $error = getSegmentError('user', $user);
    $error ??= getSegmentError('list', $list);
    if ($error) {
        err(400, $error);
    }

    $content = @file_get_contents($filename);

    if (!$content) {
        err(404, 'Listan finns inte');
    }

    if (@$_GET['text'] !== null) {
        header('Content-Type: text/plain; charset=utf-8');
        exit($content);
    }

    $lastUpdate = filemtime($filename);

    content($user, $list, $content, $lastUpdate);
} else {
    index();
}



function getSegmentError($type, $segment) {
    $typeText = $type === 'user' ? 'Användarnamn' : 'Listnamn';

    if (!$segment) {
        return "$typeText krävs";
    }

    if (strpos($segment, '  ') !== false) {
        return "$typeText får inte innehålla två mellanslag i rad";
    }

    if (strpos($segment, '/') !== false) {
        return "$typeText får inte innehålla snedstreck";
    }

    if ($type === 'user') {
        if (strlen($segment) > 20) {
            return "$typeText får max vara 20 tecken långt";
        }

        if (strpos($segment, '-') !== false) {
            return "$typeText får inte innehålla bindestreck";
        }
    } elseif ($type === 'list') {
        if (strlen($segment) > 200) {
            return "$typeText får max vara 200 tecken långt";
        }
    }

    return null;
}

function status(int $status) {
    $statuses = [
        204 => 'No Content',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        500 => 'Internal Server Error',
    ];
    $statusText = $statuses[$status];
    header("HTTP/1.1 $status $statusText");
}

function preamble($title) {
    ?>
        <!DOCTYPE html>
        <meta charset="utf-8">
        <title><?=htmlspecialchars($title)?></title>
        <style>
            *, ::before, ::after {
                box-sizing: border-box;
            }

            html {
                font-family: sans-serif;
                max-width: 60rem;
                margin: 0 auto;
                padding-bottom: 10rem;
            }

            :focus {
                border-color: #09f;
                outline: none;
            }

            input:invalid, textarea:invalid {
               border-left: 3px solid #c00;
            }

            label {
                display: block;
                margin-top: 0.5rem;
            }

            button, input {
                font: inherit;
            }

            input, textarea, button {
                border: 2px solid #999;
                border-radius: 0.3rem;
                padding: 0.5rem;
            }

            input, textarea {
                width: 100%;
            }

            textarea {
                height: 40rem;
            }

            table {
                margin: 0 -0.5rem;
            }

            td, th {
                padding: 0.2rem 0.5rem;
                text-align: left;
            }

            .comment {
                color: #666;
            }

            .inline-block {
                display: inline-block;
            }
        </style>
    <?php
}

function deleted() {
    preamble('Borttagen');
    ?>
    Borttagen. <a href="./">Startsida</a>
    <?php
}

function content($user, $list, $content, $lastUpdate) {
    preamble("$user / $list");
    $titles = explode("\n", $content);
    $titles = array_map('trim', $titles);
    ?>
    <h1>
        <a href="./">Listor</a>
        / <span class="inline-block"><?=htmlspecialchars($user)?></span>
        / <span class="inline-block"><?=htmlspecialchars($list)?></span>
    </h1>
    <table>
        <?php
            $index = 0;
            foreach ($titles as $line) {
                @[$title, $comment] = preg_split('/\s*#\s*/', $line, 2);
                $titleHtml = htmlspecialchars($title);
                if ($title) {
                    $index++;
                }

                ?>
                <tr>
                    <?php
                        if ($title) {
                            ?>
                                <td><?=$index?>.</td>
                                <td><a target="_blank" href="https://sv.wiktionary.org/wiki/<?=$titleHtml?>"><?=$titleHtml?></a></td>
                                <td><a target="_blank" href="https://sv.wiktionary.org/w/index.php?action=edit&title=<?=$titleHtml?>">Redigera</a></td>
                                <td><a target="_blank" href="https://sv.wiktionary.org/wiki/Special:Länkar_hit/<?=$titleHtml?>">Länkar hit</a></td>
                                <td class="comment"><?=htmlspecialchars($comment)?></td>
                            <?php
                        } elseif (@$comment[0] === '#') {
                            ?>
                            <td colspan="5" class="comment"><?=htmlspecialchars(substr($comment, 1))?></td>
                            <?php
                        } else {
                            ?>
                            <th colspan="5"><?=htmlspecialchars($comment)?></th>
                            <?php
                        }
                    ?>
                </tr>
                <?php
            }
        ?>
    </table>
    <form method="post" action="./">
        <h2>Uppdatera listan</h2>
        <input type="hidden" name="user" value="<?=htmlspecialchars($user)?>">
        <input type="hidden" name="list" value="<?=htmlspecialchars($list)?>">
        <textarea name="content"><?=htmlspecialchars($content)?></textarea>
        <p>
            <button type="submit" name="update" value="1" accesskey="s">Spara</button>
            <button type="submit" name="delete" value="1" onclick="return confirm('Är du säker?')">Radera</button>
        </p>
    </form>
    <footer><small>
        Senast uppdaterad <?=date('c', $lastUpdate)?>
        - <a href="./?_=<?=urlencode("$user-$list")?>&text">Visa som text</a>
    </small></footer>
    <?php
}

function index() {
    preamble("Listor");
    $lists = scandir('content');
    $lists = array_filter($lists, fn ($list) => strpos($list, '-') !== false);
    $lists = array_map(fn ($list) => basename($list, '.txt'), $lists);
    ?>
    <h1>Listor</h1>
    <ul>
        <?php
            foreach ($lists as $list) {
                // Only replace first dash.
                $formatted = preg_replace('/[-]/', ' / ', $list, 1);
                ?>
                <li><a href="?_=<?=urlencode($list)?>"><?=htmlspecialchars($formatted)?></a></li>
                <?php
            }
        ?>
    </ul>
    <form method="post" action="./">
        <h2>Skapa en lista</h2>
        <label for="user">Användarnamn</label>
        <input id="user" name="user" required maxlength="20" pattern="^[^/-]+$" title="Inga snedstreck eller bindestreck">
        <label for="list">Namn på listan</label>
        <input id="list" name="list" required maxlength="200" pattern="^[^/]+$" title="Inga snedstreck">
        <label for="content">Lista över sidnamn</label>
        <textarea id="content" name="content" required></textarea>
        <p>
            <button type="submit" name="create" value="1" accesskey="s">Skapa</button>
        </p>
    </form>
    <?php
}

function err(int $status, string $msg) {
    status($status);
    preamble('Fel');
    exit($msg);
}
