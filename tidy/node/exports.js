const init = require("../non-browser-common/fetchData.js");
const getData = require("../tidy-data.js");
const processOnSave = require("../tidy-on-save.js");

module.exports = {
  init,
  getData,
  process,
  processOnSave,
};

let data;

/**
 * @param {string} wikitext
 * @returns {{
 *  wikitext: string;
 *  transformationCategories: string[];
 *  warnings: Set<string>;
 *  context: any;
 * }}
 */
function process(wikitext) {
  data ??= getData();

  const context = processOnSave(wikitext, data);

  return {
    wikitext: context.unopaque(context.wikitext, Infinity),
    transformationCategories: context.transformCats,
    warnings: context.warnings,
    context,
  };
}
