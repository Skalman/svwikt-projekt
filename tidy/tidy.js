// @ts-check

var processOnFocus = require("./tidy-on-focus.js");
var processOnKeydown = require("./tidy-on-keydown.js");
var processOnSave = require("./tidy-on-save.js");
var processSummary = require("./tidy-on-save-summary.js");
var getData = require("./tidy-data.js");

// Export items needed for testing.
/** @type {TidyExports} */
var exports = {
  processOnSave: processOnSave,
  processOnKeydown: processOnKeydown,
  getData: getData,
};

/** @type {*} */ (window).tidy = exports;

// Declare types of global dependencies inline, to make this gadget self-contained.

/**
 * @typedef {{
 *  processOnSave: typeof processOnSave,
 *  processOnKeydown: typeof processOnKeydown,
 *  getData: typeof getData,
 * }} TidyExports
 *
 * @typedef {{
 *  on(event: string, cb: (e: Event) => void): JQuery;
 *  trigger(event: string): JQuery;
 *  find(selector: string): JQuery;
 *  each(callback: (index: string, value: HTMLElement) => void): JQuery;
 *  parent(): JQuery;
 *  parents(selector: string): JQuery;
 *  text(): string;
 *  text(str: string): JQuery;
 *  val(): string;
 *  val(x: string): JQuery;
 *  css(props: Record<string, string | number>): JQuery;
 *  prop(props: Record<string, string | number | boolean>): JQuery;
 *  prop(prop: string): string | number | boolean;
 *  addClass(x: string): JQuery;
 *  prepend(...content: (string | JQuery | (string | JQuery)[])[]): JQuery;
 *  append(...content: (string | JQuery | (string | JQuery)[])[]): JQuery;
 *  remove(): JQuery;
 *  toggle(): JQuery;
 *  slideDown(): JQuery;
 *  slideUp(): JQuery;
 *  0: HTMLElement & { CodeMirror?: CodeMirror };
 *  [index: number]: HTMLElement;
 *  length: number;
 * }} JQuery
 *
 * @typedef {{
 *  (selectorOrHtml: string, props?: { text?: string; }): JQuery;
 *  (node: HTMLElement | JQuery): JQuery;
 * }} JQueryStatic
 *
 * @typedef {{
 *  hook(name: "wikipage.content"): MwHook<[elem: JQuery]>;
 *  hook(name: "wikipage.editform"): MwHook<[elem: JQuery]>;
 *  hook(name: "ext.CodeMirror.switch"): MwHook<[isCodeMirror: boolean, elem: JQuery]>;
 *  loader: {
 *    using(dependencies: string[]): Promise<void>;
 *    implement(moduleName: string, fn: () => void): Promise<void>;
 *  };
 *  config: {
 *    get<T extends keyof MwConfig>(name: T): MwConfig[T];
 *    set<T extends keyof MwConfig>(name: T, value: MwConfig[T]): void;
 *  };
 * }} MediaWiki
 *
 * @typedef {{
 *  wgNamespaceNumber: number;
 *  wgPageName: string;
 *  wgUserGroups: ("*" | "bot")[];
 * }} MwConfig
 *
 * @typedef {{
 *  doc: {
 *    getSelection(): string;
 *    getValue(): string;
 *    setValue(x: string): void;
 *    getCursor(): "cursor"; // Not specced here.
 *    indexFromPos(cursor: "cursor"): number;
 *  };
 *  on<T extends keyof CmEventMap>(type: T, cb: CmEventMap[T]): void;
 * }} CodeMirror Selected parts of the CodeMirror 5 API.
 *
 * @typedef {{
 *  change: (instance: CodeMirror, change: CmChange) => void;
 *  focus: (instance: CodeMirror, event: Event) => void;
 *  keydown: (instance: CodeMirror, event: Event) => void;
 * }} CmEventMap
 *
 * @typedef {{
 *  origin: "+input" | "...";
 *  removed: string[];
 *  text: string[];
 *  from: unknown;
 *  to: unknown;
 * }} CmChange
 *
 * @typedef {{
 *  value: string;
 *  setValue(x: string): void;
 *  pos?: number;
 * }} NormalizedTextbox
 *
 * @typedef {{type: "warning"; message: string}} EditorWarning
 *
 * @typedef {{
 *  ui: {
 *    infuse(node: JQuery): OOElement;
 *    MessageWidget: {
 *      new (opts: {
 *        type: "success";
 *        inline: boolean;
 *        label: string;
 *      }): OOJQueryWrapper;
 *    };
 *  };
 * }} OO
 *
 * @typedef {{ $element: JQuery }} OOJQueryWrapper
 *
 * @typedef {{
 *  disabled: boolean;
 *  setDisabled(x: boolean): void;
 * }} OOElement
 *
 * @typedef {{
 *  message: string | HTMLElement | JQuery;
 *  seconds?: number;
 *  tag?: string;
 * }} SessionStorageNotifyOptions
 * @typedef {(opts: SessionStorageNotifyOptions) => void} SessionStorageNotify
 */

/**
 * @template {any[]} Args
 * @typedef {{
 *  add(cb: (...args: Args) => void): void;
 *  fire(...args: Args): void;
 * }} MwHook
 */

/** @type {MediaWiki} */
var mw = /** @type {*} */ (window).mw;

if (mw.config.get("wgNamespaceNumber") === 0) {
  mw.loader
    .using([
      "ext.gadget.editorwarnings",
      "ext.gadget.sessionStorageNotify",
      "ext.gadget.data-lang",
      "ext.gadget.data-lang-code-templates",
      "ext.gadget.data-lang-replacements",
      "ext.gadget.data-h3",
      "ext.gadget.data-headings",
      "oojs-ui-core",
    ])
    .then(function () {
      mw.hook("wikipage.editform").add(main);
    });
}

/** @param {JQuery} form */
function main(form) {
  polyfills();

  /** @type {(category: string, warnings: EditorWarning[]) => void} */
  var setEditorWarnings = /** @type {*} */ (window).setEditorWarnings;

  /** @type {OO} */
  var OO = /** @type {*} */ (window).OO;

  /** @type {SessionStorageNotify} */
  var sessionStorageNotify = /** @type {*} */ (window).sessionStorageNotify;

  if (
    ["Övriga tecken", "Övriga uppslagsord"].includes(
      mw.config.get("wgPageName")
    )
  ) {
    setEditorWarnings("tidy", [
      {
        type: "warning",
        message:
          "Finessen Tidy är avaktiverad för denna sida. Ingen automatisk korrigering av wikitext görs.",
      },
    ]);
    return;
  }

  var textbox = form.find("#wpTextbox1");

  if (/[?&]section=/.test(location.search) && /^===[^=]/.test(textbox.val())) {
    setEditorWarnings("tidy", [
      {
        type: "warning",
        message:
          "Finessen Tidy är avaktiverad eftersom du redigerar ett ordklassavsnitt. Ingen automatisk korrigering av wikitext görs.",
      },
    ]);
    return;
  }

  var data = getData();

  var save = form.find("#wpSave");
  var ooSave = OO.ui.infuse(save.parent());
  var summary = form.find("#wpSummary");

  form.find("input[type=submit]").on("click", handleClick);
  textbox
    .on("input", handleInput)
    .on("focus", handleFocus)
    .on("keydown", handleKeydown);

  var didAddCmListeners = false;

  mw.hook("ext.CodeMirror.switch").add(function (_isCm, elem) {
    textbox = elem;
    var cm = elem[0].CodeMirror;
    if (cm && !didAddCmListeners) {
      cm.on("change", handleInput);
      cm.on("focus", handleFocus);
      cm.on("keydown", function (_cm, e) {
        handleKeydown(e);
      });
      didAddCmListeners = true;
    }
  });

  updateWarnings();
  handleFocus();

  /** @type {{key: string; update: number; timeout: number} | undefined} */
  var lastWarnings;
  /** @type {number | undefined} */
  var inputTimeout;

  function handleInput() {
    clearTimeout(inputTimeout);

    if (lastWarnings) {
      updateWarnings();
    } else {
      inputTimeout = setTimeout(updateWarnings, 3000);
    }
  }

  function handleFocus() {
    var tb = getTextbox();

    var newValue = processOnFocus(tb.value);

    if (tb.value !== newValue) {
      tb.setValue(newValue);
    }
  }

  /** @param {Event} e */
  function handleClick(e) {
    var isSave = e.target === save[0];
    update(true, isSave);

    if (isSave && ooSave.disabled) {
      // It might have been disabled during the above `update()`.
      e.preventDefault();
    }
  }

  function updateWarnings() {
    update(false, false);
  }

  /**
   * @param {boolean} updateText
   * @param {boolean} isSave
   */
  function update(updateText, isSave) {
    var tb = getTextbox();
    var context = processOnSave(tb.value, data);

    if (updateText) {
      // Update textbox value.
      var updated = context.unopaque(context.wikitext, Infinity);
      if (tb.value !== updated) {
        tb.setValue(updated);
        var cats = context.transformCats;

        sessionStorageNotify({
          message: new OO.ui.MessageWidget({
            type: "success",
            inline: true,
            label:
              "Wikitexten justerad" +
              (cats.length && " (" + cats.join(", ") + ")"),
          }).$element,
          seconds: 4 + cats.length,
          tag: "tidy",
        });
      }

      // Update edit summary.
      var newSummary = processSummary(summary.val(), context, isSave);
      if (newSummary !== undefined) {
        summary.val(newSummary);
      }
    }

    // Update warnings.
    var warnArr = Array.from(context.warnings);
    setEditorWarnings(
      "tidy",
      warnArr.map(function (text) {
        return { type: "warning", message: text };
      })
    );

    var warnKey = warnArr.join("");
    if (warnKey) {
      if (!lastWarnings || lastWarnings.key !== warnKey) {
        ooSave.setDisabled(true);
        if (lastWarnings) {
          clearTimeout(lastWarnings.timeout);
        }

        lastWarnings = {
          key: warnKey,
          update: Date.now(),
          timeout: setTimeout(function () {
            ooSave.setDisabled(false);
          }, 3000),
        };
      }
    } else {
      lastWarnings = undefined;
      ooSave.setDisabled(false);
    }
  }

  /** @param {Event} e */
  function handleKeydown(e) {
    var toInsert = processOnKeydown(
      /** @type {KeyboardEvent} */ (e).key,
      function () {
        var tb = getTextbox();

        if (tb.pos === undefined) {
          return;
        }

        return {
          wikitext: tb.value,
          cursor: tb.pos,
        };
      },
      data
    );

    if (toInsert) {
      e.preventDefault();
      document.execCommand("insertText", false, toInsert);
    }
  }

  /**
   * Returns a normalized interface to a textbox.
   * @returns {NormalizedTextbox}
   */
  function getTextbox() {
    var elem = textbox[0];
    if (elem instanceof HTMLTextAreaElement) {
      return getTextboxFromTextarea(elem);
    } else if (elem.CodeMirror) {
      // It is a <div> with a CodeMirror property.
      return getTextboxFromCodeMirror(elem.CodeMirror);
    } else {
      throw new Error("Textbox invariant");
    }
  }
}

/**
 * @param {HTMLTextAreaElement} elem
 * @returns {NormalizedTextbox}
 */
function getTextboxFromTextarea(elem) {
  return {
    value: elem.value,
    setValue: function (x) {
      var prev = {
        scrollTop: elem.scrollTop,
        scrollLeft: elem.scrollLeft,
        start: elem.selectionStart,
        end: elem.selectionEnd,

        focus:
          /** @type {HTMLElement | null} */ (document.activeElement) ||
          document.body,
      };

      if (prev.focus !== elem) {
        elem.focus();
      }
      document.execCommand("selectAll");
      document.execCommand("insertText", false, x);

      elem.selectionStart = prev.start;
      elem.selectionEnd = prev.end;
      setTimeout(function () {
        elem.scrollTo(prev.scrollLeft, prev.scrollTop);
      });
      if (prev.focus !== elem) {
        prev.focus.focus();
      }
    },
    pos:
      elem.selectionStart === elem.selectionEnd
        ? elem.selectionStart
        : undefined,
  };
}

/**
 * @param {CodeMirror} cm
 * @returns {NormalizedTextbox}
 */
function getTextboxFromCodeMirror(cm) {
  return {
    value: cm.doc.getValue(),
    setValue: function (x) {
      cm.doc.setValue(x);
    },
    pos:
      cm.doc.getSelection() === ""
        ? cm.doc.indexFromPos(cm.doc.getCursor())
        : undefined,
  };
}

function polyfills() {
  // Support IE and Edge < 14.
  if (!"".includes || ![].includes) {
    String.prototype.includes = Array.prototype.includes = function (
      search,
      pos
    ) {
      return this.indexOf(search, pos) !== -1;
    };
  }

  // Support IE and Edge <= 18.
  if (!window.globalThis) {
    // @ts-expect-error
    globalThis = window;
  }

  // Support IE.
  if (!"".startsWith) {
    /**
     * @param {string} search
     * @param {number} [pos]
     */
    String.prototype.startsWith = function (search, pos) {
      return this.slice(pos, (pos || 0) + search.length) === search;
    };
  }
}
