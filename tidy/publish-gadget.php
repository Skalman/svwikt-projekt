<!DOCTYPE html>
<meta charset="utf-8" />
<title>Publish gadget</title>
<link rel="stylesheet" href="demo.css" />
<style>
    td {
        padding: 0.3em;
        vertical-align: middle;
    }
</style>
<?php
readfile('nav.html');
?>
<p>Will only work for interface admins</p>

<table>
<?php
foreach (scandir('.') as $file) {
    if (strpos($file, 'tidy') === 0) {
        $page = "MediaWiki:Gadget-$file";
        $newContent = file_get_contents($file);
        ?>
        <tr>
            <td>
                <a href="https://sv.wiktionary.org/wiki/<?= $page ?>"><?= $page ?></a>
            </td>
            <td>
                <form
                    method="post"
                    target="_blank"
                    id="<?= $page ?>"
                    action="https://sv.wiktionary.org/wiki/<?= $page ?>?action=submit"
                    class="mb-1"
                >
                    <input type="hidden" name="wpTextbox1" value="<?= htmlspecialchars($newContent) ?>">
                    <input type="submit" name="wpDiff" value="Loading..." disabled>
                    <input type="hidden" name="wpUltimateParam" value="1">
                    <script>
                        setTimeout(async () => {
                            const response = await fetch("https://sv.wiktionary.org/w/rest.php/v1/page/<?= $page ?>");
                            const data = await response.json();
                            const oldContent = (data.source || "") + "\n";
                            const newContent = document.querySelector("[id='<?= $page ?>'] [name=wpTextbox1]").value;
                            const submit = document.querySelector("[id='<?= $page ?>'] [name=wpDiff]");

                            submit.disabled = oldContent === newContent;
                            if (oldContent === newContent) {
                                submit.value = "Already up to date";
                            } else {
                                const diff = newContent.length - oldContent.length;
                                const diffStr = diff < 0 ? diff : '+' + diff;
                                submit.value = `Show diff (${diffStr})`;
                            }
                        });
                    </script>
                </form>
            </td>
        </tr>
        <?php
    }
}
?>
</table>
