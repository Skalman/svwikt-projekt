// @ts-check

/** @type {import("./tidy.js").JQueryStatic} */
const $ = /** @type {*} */ (window).$;

const getData = require("./tidy-data.js");
const processOnSave = require("./tidy-on-save.js");
const processSummary = require("./tidy-on-save-summary.js");

/** @type {import("./tidy.js").MediaWiki} */
var mw = /** @type {*} */ (window).mw;

mw.loader
  .using([
    "ext.gadget.data-lang",
    "ext.gadget.data-lang-code-templates",
    "ext.gadget.data-lang-replacements",
    "ext.gadget.data-h3",
    "ext.gadget.data-headings",
  ])
  .then(main);

function main() {
  const textbox = /** @type {HTMLTextAreaElement} */ ($("#wpTextbox1")[0]);
  const codeMirror = $(".CodeMirror [contenteditable]")[0];
  const textboxLive = /** @type {HTMLTextAreaElement} */ (
    $("#wpTextbox1-live")[0]
  );
  const techInfo = /** @type {HTMLTextAreaElement} */ ($("#tech-info")[0]);
  const warnings = $("#editorwarnings-live")[0];
  const notif = $("#notif-live")[0];

  let isCodeMirror = false;
  mw.hook("ext.CodeMirror.switch").add((isCm) => {
    isCodeMirror = isCm;
  });

  textbox.oninput = codeMirror.oninput = updateLive;
  $("#wpSummary, #bot").on("input", updateLive);
  setTimeout(updateLive);

  /** @param {string} str */
  function readableOpaque(str) {
    return str?.replace(/\0(\d+)\0/g, "❰$1❱");
  }

  /** @param {number} num */
  function round(num) {
    return Math.round(1000 * num) / 1000;
  }

  let totalProcessTime = 0;
  let totalProcessCount = 0;

  function updateLive() {
    const oldWikitext = isCodeMirror ? codeMirror.innerText : textbox.value;
    const timeBefore = performance.now();
    const context = processOnSave(oldWikitext, getData());
    const newWikitext = context.unopaque(context.wikitext, Infinity);
    const processTime = performance.now() - timeBefore;
    totalProcessTime += processTime;
    totalProcessCount++;
    const avgProcessTime = totalProcessTime / totalProcessCount;
    textboxLive.value = newWikitext;
    textboxLive.disabled = oldWikitext === newWikitext;

    techInfo.value = [
      ...context.allContexts.map(({ wikitext, replacement, creator }, i) =>
        [
          readableOpaque(replacement) + (creator ? ` - ${creator}` : ""),
          "▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁",
          "▏" + readableOpaque(wikitext).replace(/\n/g, "\n▏"),
          "▔▔▔▔▔▔▔▔▔▔▔▔▔▔▔",
        ].join("\n")
      ),
      `${round(processTime)} ms (average ${round(avgProcessTime)} ms)`,
    ]
      .join("\n\n")
      .trimStart();

    const transformCats = context.transformCats.join(", ");
    $(notif)
      .text("")
      .append(
        $("<p>", {
          text:
            "Wikitexten justerad" +
            (transformCats && " (" + transformCats + ")"),
        })
      );
    notif.hidden = oldWikitext === newWikitext;

    const warningTexts = Array.from(context.warnings);
    $(warnings)
      .text("")
      .append(warningTexts.map((x) => $("<p>", { text: x })));
    warnings.hidden = !warningTexts.length;

    const summary = $("#wpSummary").val();
    const summarySave = processSummary(summary, context, true);
    const summaryPreview = processSummary(summary, context, false);

    $("#wpSummary-live-save")
      .val(summarySave ?? summary)
      .prop({ disabled: summarySave === undefined });

    $("#wpSummary-live-preview")
      .val(summaryPreview ?? summary)
      .prop({ disabled: summaryPreview === undefined });

    console.log(context);
  }
}
