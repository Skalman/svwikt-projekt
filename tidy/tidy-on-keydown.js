// @ts-check

module.exports = processOnKeydown;

/**
 * @typedef {import("./tidy-data").Data} Data
 * @typedef {import("./tidy-data").Template} Template
 * @typedef {{
 *  wikitext: string;
 *  cursor: number;
 * }} TextboxInfo
 **

/**
 * @param {string} key
 * @param {() => TextboxInfo | undefined} getTextbox
 * @param {Data} data
 * @returns {string | undefined}
 * If the current key should be `preventDefault`ed, return the text to be
 * inserted, otherwise return `undefined`.
 */
function processOnKeydown(key, getTextbox, data) {
  if (key !== "|") return;

  var tb = getTextbox();
  if (!tb) return;

  var text = tb.wikitext;
  var cursor = tb.cursor;

  var prevLineBreak = text.lastIndexOf("\n", cursor - 1);
  prevLineBreak =
    prevLineBreak === -1
      ? // Start of text.
        0
      : // Skip past the `\n` char.
        prevLineBreak + 1;

  var nextLineBreak = text.indexOf("\n", cursor);
  if (nextLineBreak === -1) {
    nextLineBreak = text.length;
  }

  var startOfLine = text.substring(prevLineBreak, cursor);
  var endOfLine = text.substring(cursor, nextLineBreak);

  var templateStart = startOfLine.lastIndexOf("{{");
  var nextTemplateStart = endOfLine.indexOf("{{");
  var nextTemplateEnd = endOfLine.indexOf("}}");

  var missing = -1;

  // Start of line must contain a `{{`.
  // End of line can either:
  // - Not contain a `}}`, or
  // - Have a `{{` before the next `}}`.
  var canInsert =
    templateStart !== missing &&
    (nextTemplateEnd === missing ||
      (nextTemplateStart !== missing && nextTemplateStart < nextTemplateEnd));

  if (!canInsert) {
    return;
  }

  var templateName = /** @type {Template} */ (
    startOfLine.substring(templateStart + 2)
  );

  /** @type {Record<string, import("./tidy-data").LangCode>} */
  var langs = data.langCodesByUcfirstName;

  if (templateName === "ö" || templateName === "ö+") {
    langs = data.langCodesByLcName;
    var match = /^\*\*?([^:]+):/m.exec(startOfLine);

    if (!match) return;

    if (!langs.hasOwnProperty(match[1])) {
      if (!startOfLine.startsWith("**")) return;

      match = findPrev(text, /^\*([^:*]+):.*?\n/gm, cursor);
      if (!match || !langs.hasOwnProperty(match[1])) return;

      // Every line between the matching language and the cursor must be a
      // line starting with `**`. Otherwise, there is something weird going
      // on, which we shouldn't support.
      var onlySubLangsBetween = text
        .slice(match.index + match[0].length, cursor)
        .split("\n")
        .every(function (x) {
          return x.startsWith("**");
        });

      if (!onlySubLangsBetween) return;
    }

    return "|" + langs[match[1]] + "|";
  } else {
    langs = data.langCodesByUcfirstName;
    var paramInfo = data.langCodeTemplates[templateName];
    if (!paramInfo || paramInfo === "språk") return;

    var langHeader = findPrev(text, /^==([^=]+)==$/gm, cursor);
    if (!langHeader) return;
    var langName = langHeader[1];

    if (!langs.hasOwnProperty(langName)) return;

    var langCode = langs[langName];

    // Special cases for {{uttal}}.
    if (templateName === "uttal") {
      // Any language code is allowed for {{uttal}} under ====Tvärspråkligt====.
      if (langCode === "--") return;

      // 2+ parameters are required for {{uttal}}, though exactly 1 numeric.
      paramInfo = [2, 2];
    }

    var min = paramInfo[0];
    var max = paramInfo[1];
    return "|" + langCode + (max === 1 ? "}}" : min > 1 ? "|" : "");
  }
}

/**
 * @param {string} str
 * @param {RegExp} regex
 * @param {number} index
 */
function findPrev(str, regex, index) {
  /** @type {RegExpExecArray | null} */
  var prev = null;

  while (true) {
    var match = regex.exec(str);
    if (!match || index <= match.index) {
      break;
    }

    prev = match;
  }

  return prev;
}
