<?php
session_start();

if (@$_POST['wpTextbox1']) {
    $wikitext = $_SESSION['wikitext'] = $_POST['wpTextbox1'];
    $summary = $_SESSION['summary'] = $_POST['wpSummary'];
    $buttonText = @$_POST['wpSave'] ?? @$_POST['wpPreview'] ?? @$_POST['wpDiff'];

    $html = file_get_contents('demo-submitted.html');
    $html = str_replace('{{NAV}}', file_get_contents('nav.html'), $html);
    $html = str_replace('{{BUTTON_TEXT}}', htmlspecialchars($buttonText), $html);
    $html = str_replace('{{WIKITEXT}}', htmlspecialchars($wikitext), $html);
    $html = str_replace('{{SUMMARY}}', htmlspecialchars($summary), $html);
    echo $html;
} else {
    $wikitext = @$_SESSION['wikitext'] ?? file_get_contents('demo-example-wikitext.txt');
    $summary = @$_SESSION['summary'];

    $html = file_get_contents('demo.html');
    $html = str_replace('{{NAV}}', file_get_contents('nav.html'), $html);
    $html = str_replace('{{WIKITEXT}}', htmlspecialchars($wikitext), $html);
    $html = str_replace('{{SUMMARY}}', htmlspecialchars($summary), $html);
    echo $html;
}
