module.exports = processSummary;

/** @type {import("./tidy.js").MediaWiki} */
var mw = window.mw;

/**
 * @param {string} summary
 * @param {import("./tidy-on-save.js").Context} context
 * @param {boolean} isSave Whether the user clicked the button to save.
 * @returns {string | undefined}
 * If the summary was updated, it is returned, otherwise `undefined`.
 */
function processSummary(summary, context, isSave) {
  summary = summary.trim();
  var orig = summary;
  var isBot = mw.config.get("wgUserGroups").includes("bot");
  var warn = context.warnings;

  // Add a ZWNJ (zero-width non-joiner) \u200C, in order to allow users to
  // write an edit summary containing '{{tidy}}' that won't
  // automatically be removed.
  if (/{{tidy\u200C}}|Tidy:\u200C.+?\u200C/.test(summary)) {
    // Remove '{{tidy}}'.
    summary = summary
      .split(/[; -]*{{tidy\u200C}}[; -]*|[; -]*Tidy:\u200C.+?\u200C/)
      .filter(Boolean)
      .join("; ");
  }

  var isEmpty = !summary;

  if (warn.size) {
    // Add '{{tidy}}'.
    summary = (summary ? summary + "; " : "") + "{{tidy\u200C}}";
  }

  if (isSave && (isEmpty || isBot) && context.transformCats.length) {
    // Add 'Tidy: transform cat 1, transform cat 2'.
    summary =
      (summary ? summary + "; " : "") +
      "Tidy:\u200C " +
      context.transformCats.join(", ") +
      "\u200C";
  }

  return summary === orig ? undefined : summary;
}
