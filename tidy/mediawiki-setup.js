// @ts-check

export {};

// MediaWiki adds script tags that point to the current origin.
// Replace those scripts with ones that point to sv.wiktionary.org.
new MutationObserver((records) => {
  for (const record of records) {
    for (const node of record.addedNodes) {
      if (
        node instanceof HTMLScriptElement &&
        node.src.startsWith(`${location.origin}/w/load.php`)
      ) {
        const copy = document.createElement("script");
        copy.src = node.src.replace(
          location.origin,
          "https://sv.wiktionary.org"
        );
        document.head.append(copy);
      }
    }
  }
}).observe(document.head, { childList: true });

// Wait until MediaWiki has loaded.
for (let i = 1; !(/** @type {*} */ (window).mw?.loader?.using); i *= 2) {
  await new Promise((resolve) => setTimeout(resolve, i));
  if (i > 10_000) {
    throw new Error("Failed to get mw.loader.using");
  }
}

/** @type {import("./tidy.js").MediaWiki} */
var mw = /** @type {*} */ (window).mw;

/** @type {import("./tidy.js").JQueryStatic} */
var $ = /** @type {*} */ (window).$;

// Only enabled in the main namespace.
mw.config.set("wgNamespaceNumber", 0);

// Not bot-enabled initially.
mw.config.set("wgUserGroups", ["*"]);

// Fake CommonJS.
const moduleRegistry = {};
/** @type {*} */ (window).module = {};
/** @type {*} */ (window).require = (module) => {
  if (moduleRegistry[module]) {
    return moduleRegistry[module];
  }

  throw new Error(`Module ${module} not found`);
};

async function addJs(url) {
  await import(url);
  moduleRegistry[url] = /** @type {*} */ (window).module.exports;
}

await addJs("./tidy-data.js");
await addJs("./tidy-on-focus.js");
await addJs("./tidy-on-keydown.js");
await addJs("./tidy-on-save.js");
await addJs("./tidy-on-save-summary.js");
await addJs("./tidy.js");

mw.loader.implement("ext.gadget.tidy", () => {});

const { page } = /** @type {HTMLElement} */ (
  document.querySelector("[data-page]")
).dataset;

switch (page) {
  case "demo":
    // Page loaded for action=edit.
    mw.hook("wikipage.editform").fire($("#editform"));

    await addJs("./demo-live.js");
    await addJs("./demo-codemirror.js");
    await addJs("./demo-settings.js");
    break;

  case "demo-submitted":
    mw.loader.using(["ext.gadget.sessionStorageNotify"]);
    break;

  case "test":
    // Page loaded for action=view
    mw.config.set("wgPageName", "Wiktionary:Finesser/Tidy/Test");
    mw.hook("wikipage.content").fire($(".mw-parser-output"));

    await addJs("./tidy-test.js");
    break;
}
