// @ts-check

/** @type {import("./tidy.js").MediaWiki} */
var mw = /** @type {*} */ (window).mw;

/** @type {import("./tidy.js").JQueryStatic} */
var $ = /** @type {*} */ (window).$;

main();

function main() {
  const checkbox = /** @type {HTMLInputElement} */ (
    $("#syntaxhighlight").on("click", toggle)[0]
  );
  let isCm = checkbox.checked;

  const wpTextbox1 = /** @type {HTMLTextAreaElement} */ ($("#wpTextbox1")[0]);
  const $cmContainer = $(".CodeMirror");
  const cmContenteditable = $cmContainer
    .find("[contenteditable]")
    .on("blur", () => {
      wpTextbox1.value = cmContenteditable.textContent ?? "";
    })[0];

  const cm = ($cmContainer[0].CodeMirror = {
    doc: {
      getValue: () => cmContenteditable.innerText,
      setValue: (x) => {
        wpTextbox1.value = cmContenteditable.textContent = x;
      },

      // TODO Needed for transformations on keydown
      getSelection: () => (getSelection()?.isCollapsed ? "" : " "),
      getCursor: () => "cursor",
      indexFromPos: (_cursor) => {
        const s = getSelection();
        if (!s?.anchorNode) return 0;

        let totalOffset = s.anchorOffset;
        let currentNode = s.anchorNode.previousSibling;
        while (currentNode) {
          if (currentNode instanceof HTMLBRElement) {
            totalOffset++;
          } else {
            totalOffset += currentNode.textContent?.length ?? 0;
          }

          currentNode = currentNode.previousSibling;
        }

        return totalOffset;
      },
    },
    on(type, /** @type {*} */ callback) {
      $(cmContenteditable).on(
        {
          change: "input",
          focus: "focus",
          keydown: "keydown",
        }[type],
        (event) => callback(cm, event)
      );
    },
  });

  if (isCm) {
    toggle();
  }

  function toggle() {
    isCm = !isCm;
    $(wpTextbox1).toggle();
    $cmContainer.toggle();

    if (isCm) {
      cmContenteditable.textContent = wpTextbox1.value;
    } else {
      wpTextbox1.value = cmContenteditable.textContent ?? "";
    }

    mw.hook("ext.CodeMirror.switch").fire(
      isCm,
      isCm ? $cmContainer : $(wpTextbox1)
    );
  }
}
