// Needed for old versions of Node.
// Use `node-fetch@2` to use with `require()`.
const fetch = require("node-fetch");
globalThis.fetch = fetch;

const { init, process } = require("tidy");

main();

async function main() {
  await init();

  const oldWikitext = "==Svenska==\n{{subst}}";
  const result = process(oldWikitext);

  console.log({
    oldWikitext,
    newWikitext: result.wikitext,
    warnings: result.warnings,
    transformationCategories: result.transformationCategories,
  });
}
