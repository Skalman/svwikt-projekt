// @ts-check

module.exports = getData;

/**
 * @typedef {{
 *  dataLang: {
 *    main: Record<LangCode, LangNameUcfirst>;
 *    onlyEtym: Record<LangCode, LangNameUcfirst>;
 *  };
 *  dataLangCodeTemplates: Record<Template, LangCodeTemplateInfo>;
 *  dataLangReplacements: Record<LangNameLc, ReplacementExplanation>;
 *  dataH3: Record<H3Template, H3HeadingUcfirst>;
 *  dataHeadings: {
 *    h2: ["Källor"];
 *    h4: ["Översättningar"];
 *    h2Exceptions: ExceptionName[];
 *    h3Exceptions: ExceptionName[];
 *    h4Exceptions: ExceptionName[];
 *  }
 * }} DataGlobals
 *
 * @typedef {{
 *  langCodesByUcfirstName: Record<LangNameUcfirst, LangCode>;
 *  langCodesByLcName: Record<LangNameLc, LangCode>;
 *  langCodeTemplates: Record<Template, LangCodeTemplateInfo>;
 *  langReplacements: Record<LangNameLc, ReplacementExplanation>;
 *  h3TemplatesByName: Record<H3HeadingUcfirst, H3Template>;
 *  headings: DataGlobals["dataHeadings"];
 * }} Data
 *
 * @typedef {NString<1>} LangCode E.g. "sv", "en"
 * @typedef {NString<2>} LangNameUcfirst E.g. "Svenska", "Engelska"
 * @typedef {NString<3>} LangNameLc E.g. "svenska", "engelska"
 * @typedef {NString<4>} Template E.g. "adj", "tagg"
 * @typedef {NString<5>} ReplacementExplanation E.g. "''bokmål'' eller ''nynorska''"
 * @typedef {NString<6>} H3Template E.g. "adj", "subst"
 * @typedef {NString<7>} H3HeadingUcfirst E.g. "Adjektiv", "Substantiv"
 * @typedef {NString<8>} ExceptionName
 *
 * @typedef {[min: number, max: number] | "språk"} LangCodeTemplateInfo
 */

/**
 * @template X
 * @typedef {string & { "": X }} NString A "nominally" typed string.
 */

/**
 * @template {string | number} [T=string] - ok: defaults are permitted
 * @typedef {[T]} A
 */

/** @type {Data | undefined} */
var cached;

/** @returns {Data} */
function getData() {
  if (!cached) {
    /** @type {DataGlobals} */
    var w = /** @type {*} */ (globalThis);

    cached = {
      langReplacements: w.dataLangReplacements,
      langCodesByUcfirstName: flip(w.dataLang.main),
      langCodesByLcName: lcLangs(w.dataLang.main),
      langCodeTemplates: w.dataLangCodeTemplates,
      h3TemplatesByName: flip(w.dataH3),
      headings: w.dataHeadings,
    };
  }

  return cached;

  /**
   * @template {string} K
   * @template {string} V
   * @param {Record<K, V>} obj
   * @returns {Record<V, K>}
   */
  function flip(obj) {
    /** @type {*} */
    var flipped = {};
    for (var key in obj) {
      flipped[obj[key]] = key;
    }
    return flipped;
  }

  /**
   * Convert a map of languages to have the lower case language name as a key.
   * @param {Record<LangCode, LangNameUcfirst>} obj
   * @returns {Record<LangNameLc, LangCode>}
   */
  function lcLangs(obj) {
    /** @type {*} */
    var res = {};
    for (var key in obj) {
      res[obj[key].toLowerCase()] = key;
    }
    return res;
  }
}
